<?php
/*
 * $ pwd
 * {project_root}
 *
 * $ ./vendor/bin/phpunit tests/SegmenterTest
 *
 */
use PHPUnit\Framework\TestCase;

require 'src/segmenter.php';

class SegmenterTest extends TestCase
{

    public function setUp()
    {
        $this->Segmenter = new Segmenter(__DIR__.'/../src/access.log.20210114');
    }

    public function test_get_men()
    {
        $expected[0] = new stdClass();
        $expected[0]->id   = 1;
        $expected[0]->name = '太郎';
        $expected[0]->sex  = '男';
    
        $this->assertEquals($expected, $this->Segmenter->get_men());
    }

    public function test_get_list_naduke()
    {
        $expected[0] = new stdClass();
        $expected[0]->id   = 2;
        $expected[0]->name = '花子';
        $expected[0]->sex  = '女';
    
        $this->assertEquals($expected, $this->Segmenter->get_list_naduke());
    }

}
