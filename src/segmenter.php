<?php
class Segmenter
{
    private $data;

    function __construct($access_log) {
        $this->data = [];
        $arr = file(__DIR__.'/data.txt');

        foreach ($arr as $k => $v) {
            $record = explode("\t", trim($v));

            $this->data[$k] = new stdClass();
            $this->data[$k]->id   = (int)$record[0];
            $this->data[$k]->name = $record[1];
            $this->data[$k]->sex  = $record[2];
        }

        // access.log.20210114
        $this->access_log = file($access_log);
    }

    function get_all() {
        return $this->data;
    }

    function get_women() {
        $retval = [];

        foreach ($this->data as $v) {
            if ($v->sex === '女') {
                $retval[] = $v;
            }
        }

        return $retval;
    }

    function get_men() {
        $retval = [];

        foreach ($this->data as $v) {
            if ($v->sex === '男') {
                $retval[] = $v;
            }
        }

        return $retval;
    }

    function get_list_naduke() {
        $retval = [];
        $data_women = $this->get_women();

        foreach ($data_women as $k => $v) {
            if ($this->is_active($v->id)) {
                $retval[] = $v;
            }
        }
        
        return $retval;
    }

    private function is_active($id) {
        foreach ($this->access_log as $v) {
            $arr = explode(' ', $v);
            $path = $arr[6];
            parse_str(parse_url($path, PHP_URL_QUERY), $d);

            if ((int)$d['id'] === $id) {
                return TRUE;
            }
        }

        return FALSE;
    }
}
