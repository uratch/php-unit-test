<?php
/*
 * 一応、PHPUnitを使わない簡易版テストも
 *
 * shell> php test.php で動きます。
 *
 */
require 'segmenter.php';

$TestSegmenter = new TestSegmenter;
$error = 0;

if ($TestSegmenter->test_get_men() === FALSE) {
    echo "error.\n";
    ++$error;
}

if ($TestSegmenter->test_get_list_naduke() === FALSE) {
    echo "error.\n";
    ++$error;
}

if ($error === 0) {
    echo "succeeded!\n";
}

class TestSegmenter {
    private $Segmenter;

    function __construct() {
        $this->Segmenter = new Segmenter(__DIR__.'/access.log.20210114');
    }

    function test_get_men() {
        $expected[0] = new stdClass();
        $expected[0]->id   = 1;
        $expected[0]->name = '太郎';
        $expected[0]->sex  = '男';

        $actual = $this->Segmenter->get_men();

        if ($expected != $actual) {
            return FALSE;
        }
    }


    function test_get_list_naduke() {
        $expected[0] = new stdClass();
        $expected[0]->id   = 2;
        $expected[0]->name = '花子';
        $expected[0]->sex  = '女';

        $actual = $this->Segmenter->get_list_naduke();

        if ($expected != $actual) {
            return FALSE;
        }
    }
}
